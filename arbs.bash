#!/bin/bash
#title           :arbs.bash
#description     :find price differences between markets on bittrex and polo
#
#author          :Aaron Coach

# % higher on bittrex
higherThan=101

# Wrap prices in " to make jq treat them as strings instead of numbers
bittrexMarketSummary=$(curl -s https://bittrex.com/api/v1.1/public/getmarketsummaries | sed -e 's/\:\(0\.[0-9]*\)/:"\1"/g')

# Polo use _ instead of - as the seperator in market names
poloniexMarketSummary=$(curl -s https://poloniex.com/public?command=returnTicker | tr _ -)

# Loop through all bittrex BTC markets
for market in $(echo $bittrexMarketSummary | jq '.result | .[].MarketName' | tr -d \" | grep "BTC-"); do

	# If polo has current market
	if echo $poloniexMarketSummary | grep -w "$market" >/dev/null 2>&1; then

		poloniexLast=$(echo $poloniexMarketSummary | jq ".[\"$market\"] | .last" | tr -d \")
		
		bittrexLast=$(echo $bittrexMarketSummary | \
		       jq -r ".result | .[] | select(.MarketName == \"$market\") | .Last")

		# If more expensive on bittrex
		if (( $(echo "($bittrexLast / $poloniexLast * 100) > $higherThan" | bc -l) )); then

			percent=$(echo "scale=2; $bittrexLast / $poloniexLast * 100" | bc -l)
			echo "$market Polo:$poloniexLast Bittrex:$bittrexLast Percent:$percent"
		fi
	fi
done
